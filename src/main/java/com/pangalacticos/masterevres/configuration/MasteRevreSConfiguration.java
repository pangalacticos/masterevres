package com.pangalacticos.masterevres.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.pangalacticos.masterevres")
public class MasteRevreSConfiguration {
	

}