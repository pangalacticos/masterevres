package com.pangalacticos.masterevres.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "atributo")
public class Atributo {
	int id;
	String nome;
	String sigla;
	String descricao;
	boolean dinamico;
//	List<AtributoDependente> dependente = new ArrayList<AtributoDependente>();
	
//	public Atributo(int id, String nome, String sigla, String descricao, boolean dinamico, List<AtributoDependente> dependente){
//		
//		this.id = id;
//		this.nome = nome;
//		this.sigla = sigla;
//		this.descricao = descricao;
//		this.dinamico = dinamico;
//		this.dependente = dependente;
//		
//	}
	
	public Atributo(int id, String nome, String sigla, String descricao){
		
		this.id = id;
		this.nome = nome;
		this.sigla = sigla;
		this.descricao = descricao;
		this.dinamico = false;

	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isDinamico() {
		return dinamico;
	}

	public void setDinamico(boolean dinamico) {
		this.dinamico = dinamico;
	}

//	public List<AtributoDependente> getDependente() {
//		return dependente;
//	}
//
//	public void setDependente(List<AtributoDependente> dependente) {
//		this.dependente = dependente;
//	}

}
