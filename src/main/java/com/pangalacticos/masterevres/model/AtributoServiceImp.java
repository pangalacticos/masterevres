package com.pangalacticos.masterevres.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;	// acho q pode apagar posteriormente

import org.springframework.stereotype.Service;

@Service("atributoService")
public class AtributoServiceImp implements AtributoService {
	
	private static final AtomicLong counter = new AtomicLong();
    
    private static List<Atributo> atributos;
    
    static { atributos = populateDummyAtributos(); }
    
    private static List<Atributo> populateDummyAtributos(){
    	List<Atributo> atributos = new ArrayList<Atributo>();
    	
    	atributos.add(new Atributo ((int)counter.incrementAndGet(),"Dextreza","Dex",""));
    	atributos.add(new Atributo ((int)counter.incrementAndGet(),"Força","For",""));
    	atributos.add(new Atributo ((int)counter.incrementAndGet(),"Flexibilidade","Fle",""));
    	atributos.add(new Atributo ((int)counter.incrementAndGet(),"Saúde","Sau",""));
    	
    	
//    	List<AtributoDependente> tda = new ArrayList<AtributoDependente>();
//    	tda.add(new AtributoDependente(2, 1, false));
//    	atributos.add(new Atributo ((int)counter.incrementAndGet(),"Ataque","Atq"," ", true, tda));
//    	
//    	tda = new ArrayList<AtributoDependente>();
//    	tda.add(new AtributoDependente(2, (float) 1.5, false));
//    	tda.add(new AtributoDependente(4, 2, false));
//    	atributos.add(new Atributo ((int)counter.incrementAndGet(),"Defesa","Def"," ", true, tda));
//    	
//    	tda = new ArrayList<AtributoDependente>();
//    	tda.add(new AtributoDependente(1, 2, false));
//    	tda.add(new AtributoDependente(3, (float) 1.5, false));
//    	atributos.add(new Atributo ((int)counter.incrementAndGet(),"Esquiva","Esq"," ", true, tda));
//
//    	tda = new ArrayList<AtributoDependente>();
//    	tda.add(new AtributoDependente(2, 2, false));
//    	tda.add(new AtributoDependente(4, 4, false));
//    	atributos.add(new Atributo ((int)counter.incrementAndGet(),"Vida","HP"," ", true, tda));
//    	
//    	
//    	tda = new ArrayList<AtributoDependente>();
//    	tda.add(new AtributoDependente(5, 1, false));
//    	atributos.add(new Atributo ((int)counter.incrementAndGet(),"Dano","Dn"," ", true, tda));
//    	
//    	tda = new ArrayList<AtributoDependente>();
//    	tda.add(new AtributoDependente(6, 1, false));
//    	atributos.add(new Atributo ((int)counter.incrementAndGet(),"Resistência","Rs"," ", true, tda));
    	
    	
    	
    	return atributos;
    }
    
    public List<Atributo> listarAtributos(){
    	return atributos;
    }
    
    

}
