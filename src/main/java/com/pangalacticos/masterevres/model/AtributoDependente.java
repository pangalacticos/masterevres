package com.pangalacticos.masterevres.model;

public class AtributoDependente {
	int id;
	float operador;
	boolean multiadd;
	
	public AtributoDependente(int id, float operador, boolean multiadd){
		
		this.id = id;
		this.operador = operador;
		this.multiadd = multiadd;
		
	}
	
	public long getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getOperador() {
		return operador;
	}
	public void setOperador(float operador) {
		this.operador = operador;
	}
	public boolean isMultiadd() {
		return multiadd;
	}
	public void setMultiadd(boolean multiadd) {
		this.multiadd = multiadd;
	}

}
