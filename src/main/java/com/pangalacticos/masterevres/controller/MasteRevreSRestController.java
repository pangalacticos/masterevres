package com.pangalacticos.masterevres.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pangalacticos.masterevres.model.Atributo;
import com.pangalacticos.masterevres.model.AtributoService;
import com.pangalacticos.masterevres.model.Message;

@RestController
public class MasteRevreSRestController {
	
	@Autowired
    AtributoService atributoService;

	@RequestMapping("/")
	public String welcome() {//Welcome page, non-rest
		return "You should not be here.";
	}

	@RequestMapping("/hello/{player}")
	public Message message(@PathVariable String player) {//REST Endpoint.

		Message msg = new Message(player, "Hello " + player);
		return msg;
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/atributos/", method = RequestMethod.GET)
    public ResponseEntity<List<Atributo>> listarAtributos() {
        List<Atributo> atributos = atributoService.listarAtributos();
        if(atributos.isEmpty()){
            return new ResponseEntity<List<Atributo>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Atributo>>(atributos, HttpStatus.OK);
    }

	
}
